/* tc-cosmac -- Assemble code for COSMAC CDP1802
   Copyright (C) 1991-2019 Free Software Foundation, Inc.

   This file is part of GAS, the GNU Assembler.

   GAS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GAS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GAS; see the file COPYING.  If not, write to the Free
   Software Foundation, 51 Franklin Street - Fifth Floor, Boston, MA
   02110-1301, USA.  */

#include "as.h"
#include "subsegs.h"
#include "dwarf2dbg.h"

#include "opcode/cosmac.h"
#include "safe-ctype.h"
#include "elf/cosmac.h"

const char comment_chars[] = ";";
const char line_comment_chars[] = "#";
const char line_separator_chars[] = "!";

static int default_mach = bfd_mach_cosmac;

/* Like obj_elf_section, but issues a warning for new
   sections which do not have an attribute specification.  */

static void
cosmac_elf_section (int push)
{
  static const char * known_data_sections [] = { ".rodata", ".tdata", ".tbss" };
  static const char * known_data_prefixes [] = { ".debug", ".zdebug", ".gnu.warning" };
  char * saved_ilp = input_line_pointer;
  const char * name;

  name = obj_elf_section_name ();
  if (name == NULL)
    return;

  if (* input_line_pointer != ','
      && bfd_get_section_by_name (stdoutput, name) == NULL)
    {
      signed int i;

      /* Ignore this warning for well known data sections.  */
      for (i = ARRAY_SIZE (known_data_sections); i--;)
	if (strcmp (name, known_data_sections[i]) == 0)
	  break;

      if (i < 0)
	for (i = ARRAY_SIZE (known_data_prefixes); i--;)
	  if (strncmp (name, known_data_prefixes[i],
		       strlen (known_data_prefixes[i])) == 0)
	    break;

      if (i < 0)
	as_warn (_("new section '%s' defined without attributes - this might cause problems"), name);
    }

  /* FIXME: We ought to free the memory allocated by obj_elf_section_name()
     for 'name', but we do not know if it was taken from the obstack, via
     demand_copy_C_string(), or xmalloc()ed.  */
  input_line_pointer = saved_ilp;
  obj_elf_section (push);
}

/* This table describes all the machine specific pseudo-ops the assembler
   has to support.  The fields are:
   pseudo-op name without dot
   function to call to execute this pseudo-op
   Integer arg to pass to the function.  */

const pseudo_typeS md_pseudo_table[] =
{
  {"byte", cons, 1},
  {"half", cons, 2},
  {"long", cons, 4},

  {"program", s_ignore, 0},

  {"section",   cosmac_elf_section, 0},
  {"section.s", cosmac_elf_section, 0},
  {"sect",      cosmac_elf_section, 0},
  {"sect.s",    cosmac_elf_section, 0},

  {0, 0, 0}
};

const char EXP_CHARS[] = "eE";

/* Chars that mean this number is a floating point constant
   As in 0f12.456
   or    0d1.2345e12.  */
const char FLT_CHARS[] = "rRsSfFdDxXpP";

static struct hash_control *opcode_hash_control;	/* Opcode mnemonics.  */

/* This function is called once, at assembler startup time.  This
   should set up all the tables, etc. that the MD part of the assembler
   needs.  */

void
md_begin (void)
{
  const struct cosmac_opcode *p;

  if (!bfd_set_arch_mach (stdoutput, bfd_arch_cosmac, default_mach))
    as_warn (_("could not set architecture and machine"));

  opcode_hash_control = hash_new ();
  p = cosmac_opcodes;

  while (p)
    {
      int len = 0;
      const char *src = p->name;
      char *dst;

      if (src == 0)
	break;
      len = strlen (src);
      dst = XNEWVEC (char, len + 1);
      strcpy (dst, src);
      hash_insert (opcode_hash_control, dst, (char *) p);
      p++;
    }

  linkrelax = 0;
}

struct cosmac_op
{
  bfd_reloc_code_real_type reloc;
  expressionS exp;
};

static int parse_reg (char *, unsigned *);
static char *parse_exp (char *, struct cosmac_op *);

/* Try to parse a reg name.  Return the number of chars consumed.  */

static int
parse_reg (char *src, unsigned int *reg)
{
  char *end;
  int len;

  /* Cribbed from get_symbol_name.  */
  if (!is_name_beginner (*src) || *src == '\001')
    return 0;
  end = src + 1;
  while (is_part_of_name (*end) || *end == '\001')
    end++;
  len = end - src;

  if (TOLOWER (src[0]) == 'r')
    {
      if (src[1] >= '0' && src[1] <= '9')
	{
	  if (len == 3 && src[1] == '1'
	      && src[2] >= '0' && src[2] <= '5')
	    {
	      *reg = 10 + (src[2] - '0');
	      return len;
	    }
	  if (len == 2)
	    {
	      *reg = (src[1] - '0');
	      return len;
	    }
	}
    }

  return 0;
}

/* Parse an immediate or address-related constant and store it in OP. */

static char *
parse_exp (char *src, struct cosmac_op *op)
{
  char *save;

  save = input_line_pointer;
  input_line_pointer = src;
  expression (&op->exp);
  if (op->exp.X_op == O_absent)
    as_bad (_("missing operand"));
  src = input_line_pointer;
  input_line_pointer = save;

  if (*src == '@')
    {
      src++;
      if (src[0] == 'l' && src[1] == 'o')
	{
	  op->reloc = BFD_RELOC_COSMAC_LO8;
	  src += 2;
	}
      else if (src[0] == 'h' && src[1] == 'i')
	{
	  op->reloc = BFD_RELOC_COSMAC_HI8;
	  src += 2;
	}
    }
  return src;
}

/* The forms of operand:

   Rn			Register direct
   xx			immediate data
   aaaa			16 bit address
   aaaa@hi		hi 8 bit of address
   aaaa@lo		lo 8 bit of address  */

/* This is the guts of the machine-dependent assembler.  STR points to
   a machine dependent instruction.  This function is supposed to emit
   the frags/bytes it assembles.  */

void
md_assemble (char *str)
{
  char *op_start;
  char *op_end;
  struct cosmac_op operand;
  const struct cosmac_opcode *instruction;
  char c;

  /* Drop leading whitespace.  */
  while (*str == ' ')
    str++;

  /* Find the op code end.  */
  for (op_start = op_end = str;
       *op_end != 0 && *op_end != ' ';
       op_end++)
    ;

  if (op_end == op_start)
    {
      as_bad (_("can't find opcode "));
    }
  c = *op_end;

  *op_end = 0;

  instruction = (const struct cosmac_opcode *)
    hash_find (opcode_hash_control, op_start);

  if (instruction == NULL)
    {
      as_bad (_("unknown opcode"));
      return;
    }

  *op_end = c;
  while (*op_end == ' ')
    op_end++;

  char type = *instruction->args;
  if (type != 'C')
    {
      if (type == 'N')
	{
	  unsigned int regno = 0;
	  int len = parse_reg (op_end, &regno);
	  op_end += len;
	  if (len == 0 || *op_end)
	    as_bad (_("expecting register operand"));
	  char *f = frag_more (1);
	  char byte = (instruction->match & instruction->mask) | regno;
	  number_to_chars_bigendian (f, byte, 1);
	}
      else if (type == 'P')
	{
	  unsigned int port = *op_end - '0';
	  op_end++;
	  if (port > 7 || *op_end)
	    {
	      as_bad (_("expecting port number"));
	      port = 0;
	    }
	  char *f = frag_more (1);
	  char byte = (instruction->match & instruction->mask) | port;
	  number_to_chars_bigendian (f, byte, 1);
	}
      else if (type == 'S' || type == 'I')
	{
	  fixS *fixp;
	  char *f = frag_more (2);
	  number_to_chars_bigendian (f, instruction->match, 1);

	  f = f + 1;
	  operand.reloc = BFD_RELOC_8;
	  op_end = parse_exp (op_end, &operand);
	  fixp = fix_new_exp (frag_now, f - frag_now->fr_literal, 1,
                              &operand.exp, FALSE, operand.reloc);
	  if (operand.reloc != BFD_RELOC_8)
            fixp->fx_no_overflow = 1;
	  number_to_chars_bigendian (f, 0, 1);
	}
      else if (type == 'L')
	{
	  fixS *fixp;
	  char *f = frag_more (3);
	  number_to_chars_bigendian (f, instruction->match, 1);

	  f = f + 1;
	  operand.reloc = BFD_RELOC_16;
	  op_end = parse_exp (op_end, &operand);
	  fixp = fix_new_exp (frag_now, f - frag_now->fr_literal, 2,
                              &operand.exp, FALSE, operand.reloc);
	  if (operand.reloc != BFD_RELOC_16)
            fixp->fx_no_overflow = 1;
	  number_to_chars_bigendian (f, 0, 2);
	}
    }
  else
    {
      char *f = frag_more (1);
      number_to_chars_bigendian (f, instruction->match, 1);
    }

  if (*op_end && !ISSPACE (*op_end))
    as_bad ("junk at end of line, first unrecognized character is `%c'",
	    *op_end);

  dwarf2_emit_insn (instruction->len);
}

symbolS *
md_undefined_symbol (char *name ATTRIBUTE_UNUSED)
{
  return 0;
}

/* Various routines to kill one day.  */

const char *
md_atof (int type, char *litP, int *sizeP)
{
  return ieee_md_atof (type, litP, sizeP, TRUE);
}


const char *md_shortopts = "";
struct option md_longopts[] =
{
  {NULL, no_argument, NULL, 0}
};

size_t md_longopts_size = sizeof (md_longopts);

int
md_parse_option (int c ATTRIBUTE_UNUSED, const char *arg ATTRIBUTE_UNUSED)
{
  switch (c)
    {
    default:
      return 0;
    }
  return 1;
}

void
md_show_usage (FILE *stream ATTRIBUTE_UNUSED)
{
}

void tc_aout_fix_to_chars (void);

void
tc_aout_fix_to_chars (void)
{
  printf (_("call to tc_aout_fix_to_chars \n"));
  abort ();
}

void
md_convert_frag (bfd *headers ATTRIBUTE_UNUSED,
		 segT seg ATTRIBUTE_UNUSED,
		 fragS *fragP ATTRIBUTE_UNUSED)
{
  printf (_("call to md_convert_frag \n"));
  abort ();
}

valueT
md_section_align (segT segment, valueT size)
{
  int align = bfd_get_section_alignment (stdoutput, segment);
  return ((size + (1 << align) - 1) & (-1U << align));
}

void
md_apply_fix (fixS *fixP, valueT *valP, segT seg ATTRIBUTE_UNUSED)
{
  char *buf = fixP->fx_where + fixP->fx_frag->fr_literal;
  long val = *valP;

  switch (fixP->fx_size)
    {
    case 1:
      *buf++ = val;
      break;
    case 2:
      *buf++ = (val >> 8);
      *buf++ = val;
      break;
    default:
      abort ();
    }

  if (fixP->fx_addsy == NULL && fixP->fx_pcrel == 0)
    fixP->fx_done = 1;
}

int
md_estimate_size_before_relax (fragS *fragP ATTRIBUTE_UNUSED,
			       segT segment_type ATTRIBUTE_UNUSED)
{
  printf (_("call to md_estimate_size_before_relax \n"));
  abort ();
}

/* Put number into target byte order.  */
void
md_number_to_chars (char *ptr, valueT use, int nbytes)
{
  number_to_chars_bigendian (ptr, use, nbytes);
}

long
md_pcrel_from (fixS *fixp)
{
  as_bad_where (fixp->fx_file, fixp->fx_line,
		_("Unexpected reference to a symbol in a non-code section"));
  return 0;
}

arelent *
tc_gen_reloc (asection *section ATTRIBUTE_UNUSED, fixS *fixp)
{
  arelent *rel;
  bfd_reloc_code_real_type r_type;

  if (fixp->fx_addsy && fixp->fx_subsy)
    {
      if ((S_GET_SEGMENT (fixp->fx_addsy) != S_GET_SEGMENT (fixp->fx_subsy))
	  || S_GET_SEGMENT (fixp->fx_addsy) == undefined_section)
	{
	  as_bad_where (fixp->fx_file, fixp->fx_line,
			_("Difference of symbols in different sections is not supported"));
	  return NULL;
	}
    }

  rel = XNEW (arelent);
  rel->sym_ptr_ptr = XNEW (asymbol *);
  *rel->sym_ptr_ptr = symbol_get_bfdsym (fixp->fx_addsy);
  rel->address = fixp->fx_frag->fr_address + fixp->fx_where;
  rel->addend = fixp->fx_offset;

  r_type = fixp->fx_r_type;

#define DEBUG 0
#if DEBUG
  fprintf (stderr, "%s\n", bfd_get_reloc_code_name (r_type));
  fflush (stderr);
#endif
  rel->howto = bfd_reloc_type_lookup (stdoutput, r_type);
  if (rel->howto == NULL)
    {
      as_bad_where (fixp->fx_file, fixp->fx_line,
		    _("Cannot represent relocation type %s"),
		    bfd_get_reloc_code_name (r_type));
      return NULL;
    }

  return rel;
}
