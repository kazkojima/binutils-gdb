/* COSMAC CDP1802 opcode list
   Copyright (C) 2011-2019 Free Software Foundation, Inc.

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING3. If not,
   see <http://www.gnu.org/licenses/>.  */

#include "sysdep.h"
#include "opcode/cosmac.h"
#include <stdio.h>

/* Register names used by gas and objdump.  */

const char * const cosmac_gpr_names_numeric[NGPR] =
{
  "r0",   "r1",   "r2",   "r3",   "r4",   "r5",   "r6",   "r7",
  "r8",   "r9",   "r10",  "r11",  "r12",  "r13",  "r14",  "r15"
};

static int
match_opcode (const struct cosmac_opcode *op, insn_t insn)
{
  return ((insn ^ op->match) & op->mask) == 0;
}

#define cat(a,b) a ## b
#define xcat(a,b) cat(a,b)

#define C_C 1
#define C_N 1
#define C_P 1
#define C_I 2
#define C_S 2
#define C_L 3

const struct cosmac_opcode cosmac_opcodes[] =
{
#define DECLARE_INSN(str,arg,match,mask) \
  { #str , xcat(C_,arg) , #arg , match , mask , match_opcode, 0 },
#include "opcode/cosmac-opc.h"
  { NULL, 0, NULL, 0, 0, NULL, 0 }
};
