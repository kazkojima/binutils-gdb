/* BFD library support routines for COSMAC CDP1802 architecture.
   Copyright (C) 1990-2019 Free Software Foundation, Inc.

   This file is part of BFD, the Binary File Descriptor library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#include "sysdep.h"
#include "bfd.h"
#include "libbfd.h"

/* This routine is provided two arch_infos and works out the machine
   which would be compatible with both and returns a pointer to its
   info structure.  */

static const bfd_arch_info_type *
compatible (const bfd_arch_info_type *in, const bfd_arch_info_type *out)
{
  /* It's really not a good idea to mix and match modes.  */
  if (in->mach != out->mach)
    return 0;
  else
    return in;
}

const bfd_arch_info_type bfd_cosmac_arch =
{
  8,				/* 8 bits in a word.  */
  16,				/* 16 bits in an address.  */
  8,				/* 8 bits in a byte.  */
  bfd_arch_cosmac,
  bfd_mach_cosmac,
  "cosmac",			/* Architecture name.  */
  "cosmac",			/* Printable name.  */
  1,
  TRUE,				/* The default machine.  */
  compatible,
  bfd_default_scan,
  bfd_arch_default_fill,
  NULL
};
