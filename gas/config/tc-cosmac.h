/* This file is tc-cosmac.h
   Copyright (C) 1987-2019 Free Software Foundation, Inc.

   This file is part of GAS, the GNU Assembler.

   GAS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GAS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GAS; see the file COPYING.  If not, write to the Free
   Software Foundation, 51 Franklin Street - Fifth Floor, Boston, MA
   02110-1301, USA.  */

#define TC_COSMAC

#define TARGET_BYTES_BIG_ENDIAN 1

#define TARGET_ARCH bfd_arch_cosmac

/* Fixup debug sections since we will never relax them.  */
#define TC_LINKRELAX_FIXUP(seg) (seg->flags & SEC_ALLOC)
#define TARGET_FORMAT "elf32-cosmac"
#define LOCAL_LABEL_PREFIX '.'
#define LOCAL_LABEL(NAME) (NAME[0] == '.' && NAME[1] == 'L')
#define FAKE_LABEL_NAME ".L0\001"

struct fix;
struct internal_reloc;

#define WORKING_DOT_WORD

/* No shared lib support, so we don't need to ensure externally
   visible symbols can be overridden.  */
#define EXTERN_FORCE_RELOC 0

/* Minimum instruction is of 8 bits.  */
#define DWARF2_LINE_MIN_INSN_LENGTH 1
#define DWARF2_USE_FIXED_ADVANCE_PC 0

/* We do not want to adjust any relocations to make implementation of
   linker relaxations easier.  */
#define tc_fix_adjustable(FIX) 0

#define LISTING_HEADER "COSMAC CDP1802 GAS "

#define md_operand(x)

/* This target is buggy, and sets fix size too large.  */
#define TC_FX_SIZE_SLACK(FIX) 1
