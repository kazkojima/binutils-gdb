/* COSMAC CDP1802 disassembler
   Copyright (C) 2011-2019 Free Software Foundation, Inc.

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING3. If not,
   see <http://www.gnu.org/licenses/>.  */

#include "sysdep.h"
#include "disassemble.h"
#include "libiberty.h"
#include "opcode/cosmac.h"
#include "opintl.h"
#include "elf-bfd.h"
#include "elf/cosmac.h"

#include "bfd_stdint.h"
#include <ctype.h>
#include <assert.h>

struct cosmac_private_data
{
  bfd_vma print_addr;
  int print_value;
};

static const char * const *cosmac_gpr_names = cosmac_gpr_names_numeric;

static void
set_default_cosmac_dis_options (void)
{
}

static void
parse_cosmac_dis_option (const char *option)
{
  /* xgettext:c-format */
  opcodes_error_handler (_("unrecognized disassembler option: %s"), option);
}

static void
parse_cosmac_dis_options (const char *opts_in)
{
  char *opts = xstrdup (opts_in), *opt = opts, *opt_end = opts;

  set_default_cosmac_dis_options ();

  for ( ; opt_end != NULL; opt = opt_end + 1)
    {
      if ((opt_end = strchr (opt, ',')) != NULL)
	*opt_end = 0;
      parse_cosmac_dis_option (opt);
    }

  free (opts);
}

/* Print insn arguments.  */

static void
print_insn_args (const char *d, insn_t l, bfd_vma pc ATTRIBUTE_UNUSED,
		 disassemble_info *info)
{
  struct cosmac_private_data *pd = info->private_data;
  int rd = l & 0x0f;
  fprintf_ftype print = info->fprintf_func;

  if (*d != '\0')
    print (info->stream, "\t");

  for (; *d != '\0'; d++)
    {
      switch (*d)
	{
	case 'C':
	  /* no args */
	  break;

	case 'N':
	  print (info->stream, "%s", cosmac_gpr_names[rd]);
	  break;

	case 'P':
	  /* port number */
	  print (info->stream, "%d", l & 0x7);
	  break;

	case 'I':
	  print (info->stream, "0x%02x", pd->print_value & 0xff);
	  break;

	case 'S':
	case 'L':
	  info->target = pd->print_addr;
	  (*info->print_address_func) (info->target, info);
	  break;

	default:
	  /* xgettext:c-format */
	  print (info->stream, _("# internal error, undefined modifier (%c)"),
		 *d);
	  return;
	}
    }
}

static const struct cosmac_opcode *
find_insn (insn_t word)
{
  const struct cosmac_opcode *op;
  for (op = cosmac_opcodes; op->name; op++)
    if ((word & op->mask) == op->match)
      return op;
  /* Never */
  assert (0);
}

/* Print the COSMAC instruction at address MEMADDR in debugged memory,
   on using INFO.  Returns length of the instruction, in bytes. */

static int
cosmac_disassemble_insn (bfd_vma memaddr, disassemble_info *info)
{
  const struct cosmac_opcode *op;
  static bfd_boolean init = 0;
  static const struct cosmac_opcode *cosmac_map[256];
  struct cosmac_private_data *pd;
  insn_t word;
  int insnlen;
  int status;
  bfd_byte packet[3];

  /* Build a table to shorten the search time.  */
  if (! init)
    {
      int i;

      for (i = 0; i < 256; i++)
	cosmac_map[i] = find_insn((insn_t) i);

      init = 1;
    }

  if (info->private_data == NULL)
    {
      pd = info->private_data = xcalloc (1, sizeof (struct cosmac_private_data));
      pd->print_addr = -1;
    }
  else
    pd = info->private_data;

  status = (*info->read_memory_func) (memaddr, packet, 1, info);
  if (status != 0)
    {
      (*info->memory_error_func) (status, memaddr, info);
      return status;
    }

  word = packet[0];
  op = cosmac_map[word];
  insnlen = op->len;
  if (insnlen > 1)
    {
      status = (*info->read_memory_func) (memaddr+1, packet, insnlen-1, info);
      if (status != 0)
	{
	  (*info->memory_error_func) (status, memaddr+1, info);
	  return status;
	}
      if (*(op->args) == 'S')
	pd->print_addr = (memaddr & 0xff00) | packet[0];
      else if (*(op->args) == 'L')
	pd->print_addr = (packet[0] << 8) | packet[1];
      else /* if (*(op->args) == 'I') */
	pd->print_value = packet[0];
    }

  info->bytes_per_chunk = 1;
  info->bytes_per_line = 4;
  info->display_endian = BFD_ENDIAN_BIG;
  info->insn_info_valid = 1;
  info->branch_delay_insns = 0;
  info->data_size = 0;
  info->insn_type = dis_nonbranch;
  info->target = 0;
  info->target2 = 0;

  for (; op->name; op++)
    {
      /* Does the opcode match?  */
      if (! (op->match_func) (op, word))
	continue;

      /* It's a match.  */
      (*info->fprintf_func) (info->stream, "%s", op->name);
      print_insn_args (op->args, word, memaddr, info);

      /* Try to disassemble multi-instruction addressing sequences.  */
      if (pd->print_addr != (bfd_vma)-1)
	{
	  info->target = pd->print_addr;
	  (*info->fprintf_func) (info->stream, " # ");
	  (*info->print_address_func) (info->target, info);
	  pd->print_addr = -1;
	}

      return insnlen;
    }

  /* We did not find a match, so just print the instruction bits.  */
  info->insn_type = dis_noninsn;
  (*info->fprintf_func) (info->stream, "0x%llx", (unsigned long long)word);
  return insnlen;
}

int
print_insn_cosmac (bfd_vma memaddr, struct disassemble_info *info)
{
  if (info->disassembler_options != NULL)
    {
      parse_cosmac_dis_options (info->disassembler_options);
      /* Avoid repeatedly parsing the options.  */
      info->disassembler_options = NULL;
    }

  return cosmac_disassemble_insn (memaddr, info);
}

#if 0
/* Prevent use of the fake labels that are generated as part of the DWARF
   and for relaxable relocations in the assembler.  */

bfd_boolean
cosmac_symbol_is_valid (asymbol * sym,
                       struct disassemble_info * info ATTRIBUTE_UNUSED)
{
  const char * name;

  if (sym == NULL)
    return FALSE;

  name = bfd_asymbol_name (sym);

  return (strcmp (name, COSMAC_FAKE_LABEL_NAME) != 0);
}

void
print_cosmac_disassembler_options (FILE *stream)
{
}
#endif
