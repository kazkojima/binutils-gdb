/* COSMAC CDP1802 ELF support for BFD.
   Copyright (C) 2001-2019 Free Software Foundation, Inc.

   This file is part of BFD, the Binary File Descriptor library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.  */

#ifndef _ELF_COSMAC_H
#define _ELF_COSMAC_H

#include "elf/reloc-macros.h"

/* Relocations.  */
/* Relocations 59..64 are GNU extensions.  */
START_RELOC_NUMBERS (elf_cosmac_reloc_type)
  RELOC_NUMBER (R_COSMAC_NONE, 0)
  RELOC_NUMBER (R_COSMAC_DIR8, 1)
  RELOC_NUMBER (R_COSMAC_DIR16, 2)
  RELOC_NUMBER (R_COSMAC_LO8, 3)
  RELOC_NUMBER (R_COSMAC_HI8, 4)
END_RELOC_NUMBERS (R_COSMAC_max)

/* Machine variant if we know it.  This field was invented at Cygnus,
   but it is hoped that other vendors will adopt it.  If some standard
   is developed, this code should be changed to follow it. */

#define EF_COSMAC_MACH		0x00FF0000

#define E_COSMAC_MACH_COSMAC1802	0x00800000
#define E_COSMAC_MACH_COSMAC1805	0x00810000

#endif
